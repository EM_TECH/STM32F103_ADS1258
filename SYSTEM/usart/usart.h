#ifndef __USART_H
#define __USART_H

#include "stdio.h"  
#include "sys.h" 

/* 串口1初始化 */
void uart1_init(u32 bound);

/* 串口1发送指定字节的数据 */
void uart1_send_bytes(uint8_t *p_buf, uint32_t bytes);
    
#endif


/* end of file */
