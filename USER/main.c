#include "led.h"
#include "delay.h"
#include "sys.h"
#include "usart.h"
#include "timer.h"
#include "spi.h"
#include "ADS1256.h"
#include "mcu_adc.h"
#include "data_trans.h"

uint8_t trans_mode = 0;    /* 串口发送的帧模式， 0: 帧类型0XFA 单端型, 1 : 帧类型0XFB 不转换, 2: 帧类型0XFC 差分型*/
uint8_t ads_cfg_done = 0;  /* 标记是否已经配置过ADS1256 */
uint8_t ch_num;            /* 转换的通道数 */
int32_t ads_adc[8];        /* 存储ADS1256单端采样值 */
int32_t ads_diff_adc[4];   /* 存储ADS1256差分采样值 */
float ads_volt[8];         /* 存储ADS1256采样转换成的电压值 */
float mcu_ad_volt[2];      /* MCU AD采样转换成的电压值 */

/* 获取传感器1的温度值,对应MCU PA3  */
int8_t get_sensor1_temp(void)
{
    int8_t temp = 0;
    
    //温度转换算法...
    temp = mcu_tin_ad[0] >> 8;
    
    return temp;
}

/* 获取传感器2的温度值,对应MCU PA4  */
int8_t get_sensor2_temp(void)
{
    int8_t temp = 0;
    
    //温度转换算法...
    temp = mcu_tin_ad[1] >> 8;
    
    return temp;
}

int main(void)
{       
    int i = 0;
    delay_init();              /* 延时函数初始化 */
    NVIC_Configuration();      /* 设置NVIC中断分组2:2位抢占优先级，2位响应优先级 */
    uart1_init(230400);        /* 串口1初始化为230400 */
    ADC1_Init();               /* MCU ADC1初始化 */
    ADS1256_Init();            /* ADS1256初始化 */ 
    delay_ms(500);             /* 等待上电稳定，等基准电压电路稳定 */
    trans_mode = 2;            /* 开机默认发送0XFC帧类型 */
    
    while(1)
    {
 
        if (trans_mode == 0) {  /* 单端型 */
            
            if (ads_cfg_done == 0) {
                ads_cfg_done = 1;  /* 标记已经配置 */
                ADS1256_CfgADC(ADS1256_GAIN_1, ADS1256_3750SPS);  /* 配置ADC参数： 增益1:1, 数据输出速率 3750SPS */
                
                /*
                   中断服务程序会自动读取ADC结果保存在全局变量，主程序通过 ADS1256_GetAdc() 函数来读取这些数据
                */
                ADS1256_StartScan(0);   /* 启动中断扫描模式. 0表示单端8路 */
                ch_num = 4;             /* 通道数4 (还有4个通道没有使用) */
            }
            
            for (i = 0; i < ch_num; i++) {
                ads_adc[i] =  ADS1256_GetAdc(i);                      /* ADS1256单端采集的AD原始数据 */
                ads_volt[i] = ((float)ads_adc[i] * 2.5) / 4205800.0;  /* 转换成单端电压值 */
            }
            
            mcu_ad_volt[0] = (mcu_tin_ad[0] * 3.30) / 4096;    /* MCU  PA3采集的电压 */
            mcu_ad_volt[1] = (mcu_tin_ad[1] * 3.30) / 4096;    /* MCU  PA4采集的电压 */
            
            /* 通过串口将ADC（0、1、2、3、4、5路）单端采样值的对应电压值发送出去 */    
            __set_PRIMASK(1); 
            data_trans_dingle_ended(ads_volt, mcu_ad_volt);
            __set_PRIMASK(0); 
  
        }  else {  /* 需要采集差分信号 */
           
            if (ads_cfg_done == 0) {
                ads_cfg_done = 1;  /* 标记已经配置 */
                
                ADS1256_CfgADC(ADS1256_GAIN_1, ADS1256_30000SPS);  /* 配置ADC参数： 增益1:1, 数据输出速率 30000SPS */
                ADS1256_StartScan(1);   /* 启动中断扫描模式. 1表示差分4路 */
                ch_num = 2;     /* 通道数 = 2 ,因为只用两个差分通道 */
            }

            if (trans_mode == 1) {  
                __set_PRIMASK(1);   
                ads_diff_adc[0] = (int32_t)g_tADS1256.AdcNow[0];   /* ADS1256差分通道0的AD原始数据 */
                ads_diff_adc[1] = (int32_t)g_tADS1256.AdcNow[1];   /* ADS1256差分通道1的AD原始数据 */
                    
                /* 帧类型0XFB 不转换  */                
                data_trans_not_convert(ads_diff_adc, get_sensor1_temp(), get_sensor2_temp());
                __set_PRIMASK(0); 
                
            } else if (trans_mode == 2) {
             
                ads_diff_adc[0] = ADS1256_GetAdc(0);
                ads_diff_adc[1] = ADS1256_GetAdc(1);

                ads_volt[0] = ((float)ads_diff_adc[0] * 2.5) / 4205800.0;  /* 原始差分值转换成电压值 */
                ads_volt[1] = ((float)ads_diff_adc[1] * 2.5) / 4205800.0;  /* 原始差分值转换成电压值 */
      
                /* 帧类型0XFC  差分型 */
                __set_PRIMASK(1); 
                data_trans_difference(ads_volt, get_sensor1_temp(), get_sensor2_temp());
                __set_PRIMASK(0); 
            }                
        }
    }    
}    
 
/* end of file */
