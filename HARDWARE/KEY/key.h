#ifndef __KEY_H
#define __KEY_H  
#include "sys.h"


/* 定义按键状态 */
#define KEY_RELEASE   (0)
#define KEY_PRESS     (1)


/* 读取KEY0按键电平 */
#define KEY0  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_5)

/* 按键IO初始化 */
void KEY_Init(void);

/* 按键扫描函数 */
u8 KEY_Scan(u8);                

#endif
