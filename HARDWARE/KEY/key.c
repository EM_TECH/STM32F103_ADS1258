#include "key.h"
#include "sys.h" 
#include "delay.h"

//按键初始化函数
void KEY_Init(void) 
{ 
    GPIO_InitTypeDef GPIO_InitStructure;
    
    //初始化KEY0-->GPIOC5 上拉输入
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);//使能PORTC时钟

    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_5;//PC5
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;  //设置成上拉输入
    GPIO_Init(GPIOC, &GPIO_InitStructure);  //初始化GPIOC5
}


//按键处理函数
//返回按键值
//mode:0,不支持连续按;1,支持连续按;
//0，没有任何按键按下
//1，KEY0按下
u8 KEY_Scan(u8 mode)
{    
    static u8 key_up = 1;//按键按松开标志
    if(mode) {
        key_up = 1;     //支持连按         
    }
    if (key_up && (KEY0 == 0)) {
        delay_ms(10);   //去抖动 
        key_up = 0;
        if (KEY0 == 0) {
            return KEY_PRESS;
        }
    } else if (KEY0 == 1) {
        key_up=1;       
    }
    return KEY_RELEASE;// 无按键按下
}
