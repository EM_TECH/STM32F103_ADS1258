
#include <stdio.h>
#include "stm32f10x_gpio.h"
#include "ADS1256.h"
#include "spi.h"
#include "delay.h"

ADS1256_VAR_T g_tADS1256;

/* 寄存器定义： Table 23. Register Map --- ADS1256数据手册第30页 */
enum
{
    /* 寄存器地址， 后面是复位后缺省值 */
    REG_STATUS = 0, // x1H
    REG_MUX    = 1, // 01H
    REG_ADCON  = 2, // 20H
    REG_DRATE  = 3, // F0H
    REG_IO     = 4, // E0H
    REG_OFC0   = 5, // xxH
    REG_OFC1   = 6, // xxH
    REG_OFC2   = 7, // xxH
    REG_FSC0   = 8, // xxH
    REG_FSC1   = 9, // xxH
    REG_FSC2   = 10, // xxH
};

/* 命令定义： TTable 24. Command Definitions --- ADS1256数据手册第34页 */
enum
{
    CMD_WAKEUP  = 0x00, // Completes SYNC and Exits Standby Mode 0000  0000 (00h)
    CMD_RDATA   = 0x01, // Read Data 0000  0001 (01h)
    CMD_RDATAC  = 0x03, // Read Data Continuously 0000   0011 (03h)
    CMD_SDATAC  = 0x0F, // Stop Read Data Continuously 0000   1111 (0Fh)
    CMD_RREG    = 0x10, // Read from REG rrr 0001 rrrr (1xh)
    CMD_WREG    = 0x50, // Write to REG rrr 0101 rrrr (5xh)
    CMD_SELFCAL = 0xF0, // Offset and Gain Self-Calibration 1111    0000 (F0h)
    CMD_SELFOCAL= 0xF1, // Offset Self-Calibration 1111    0001 (F1h)
    CMD_SELFGCAL= 0xF2, // Gain Self-Calibration 1111    0010 (F2h)
    CMD_SYSOCAL = 0xF3, // System Offset Calibration 1111   0011 (F3h)
    CMD_SYSGCAL = 0xF4, // System Gain Calibration 1111    0100 (F4h)
    CMD_SYNC    = 0xFC, // Synchronize the A/D Conversion 1111   1100 (FCh)
    CMD_STANDBY = 0xFD, // Begin Standby Mode 1111   1101 (FDh)
    CMD_RESET   = 0xFE, // Reset to Power-Up Values 1111   1110 (FEh)
};

/* 速率配置表 */
static const uint8_t s_tabDataRate[ADS1256_DRATE_MAX] =
{
    0xF0,       /* 复位时缺省值 */
    0xE0,
    0xD0,
    0xC0,
    0xB0,
    0xA1,
    0x92,
    0x82,
    0x72,
    0x63,
    0x53,
    0x43,
    0x33,
    0x20,
    0x13,
    0x03
};


/* 驱动延时us函数 */
static void bsp_DelayUS(u16 nus)
{
    delay_us(nus);
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_DelayDATA
*   功能说明: 读取DOUT之前的延迟
*   形    参: 无
*   返 回 值: 无
*********************************************************************************************************
*/
static void ADS1256_DelayDATA(void)
{
    /*
        Delay from last SCLK edge for DIN to first SCLK rising edge for DOUT: RDATA, RDATAC,RREG Commands
        最小 50 个tCLK = 50 * 0.13uS = 6.5uS
    */
    bsp_DelayUS(10);    /* 最小延迟 6.5uS, 此处取10us */
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_StartScan
*   功能说明: 将 DRDY引脚 （PH9 ）配置成外部中断触发方式， 中断服务程序中扫描8个通道的数据。
*   形    参: _ucDiffMode : 0 表示单端模式（扫描8路）； 1表示差分模式，扫描4路
*   返 回 值: 无
*********************************************************************************************************
*/
void ADS1256_StartScan(uint8_t _ucScanMode)
{
    EXTI_InitTypeDef   EXTI_InitStructure;
    NVIC_InitTypeDef   NVIC_InitStructure;

    g_tADS1256.ScanMode = _ucScanMode;
    /* 开始扫描前, 清零结果缓冲区 */
    {
        uint8_t i;

        g_tADS1256.Channel = 0;

        for (i = 0; i < 8; i++)
        {
            g_tADS1256.AdcNow[i] = 0;
        }
    }

    /* Enable AFIO clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

    /* Connect EXTI10 Line to Pb10 pin */
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource10);

    /* Configure EXTI10 line */
    EXTI_InitStructure.EXTI_Line = EXTI_Line10;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  /* 下降沿 */
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    /* Enable and set EXTI3 Interrupt  priority */
    NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_StopScan
*   功能说明: 停止 DRDY 中断
*   形    参: 无
*   返 回 值: 无
*********************************************************************************************************
*/
void ADS1256_StopScan(void)
{
    EXTI_InitTypeDef   EXTI_InitStructure;
    NVIC_InitTypeDef   NVIC_InitStructure;

    
    /* 配置 EXTI LineXXX */
    EXTI_InitStructure.EXTI_Line = EXTI_Line10;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; /* 下降沿(等待 DRDY 由1变0的时刻) */
    EXTI_InitStructure.EXTI_LineCmd = DISABLE;      /* 禁止 */
    EXTI_Init(&EXTI_InitStructure);

    /* 中断优先级配置 最低优先级 这里一定要分开的设置中断，不能够合并到一个里面设置 */
    NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;        /* 禁止 */
    NVIC_Init(&NVIC_InitStructure);
}

/*
*********************************************************************************************************
*   函 数 名: Init_ADS1256_GPIO
*   功能说明: 初始化ADS1256 GPIO。
*   形    参: 无
*   返 回 值: 无
*********************************************************************************************************
*/
void Init_ADS1256_GPIO(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    /* _DRDY引脚需要设置为浮空状态 */
    GPIO_InitStructure.GPIO_Pin =  PIN_DRDY;            
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;         
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;        
    GPIO_Init(PORT_DRDY, &GPIO_InitStructure);                  
    GPIO_ResetBits(PORT_DRDY, PIN_DRDY);      
    
    /* PB12 CS */
    GPIO_InitStructure.GPIO_Pin = PIN_CS;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(PORT_CS, &GPIO_InitStructure);
    GPIO_SetBits(PORT_CS, PIN_CS);
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_WaitDRDY
*   功能说明: 等待内部操作完成。 自校准时间较长，需要等待。
*   形    参: 无
*   返 回 值: 无
*********************************************************************************************************
*/
static void ADS1256_WaitDRDY(void)
{
    uint32_t i;

    for (i = 0; i < 40000000; i++)
    {
        if (DRDY_IS_LOW())
        {
            break;
        }
    }
    if (i >= 40000000)
    {
//      printf("ADS1256_WaitDRDY() Time Out ...\r\n");      /* 调试语句. 用语排错 */
    }
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_ResetHard
*   功能说明: 硬件初始化
*   形    参: 无
*   返 回 值: 无
*********************************************************************************************************/
void ADS1256_ResetHard(void)
{
    
    ADS1256_WaitDRDY();
    CS_0(); /* SPI片选 = 0 */
    SPI2_ReadWriteByte(CMD_RESET);   
    CS_1(); /* SPI片选 = 1 */
    delay_ms(2);
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_CfgADC
*   功能说明: 配置ADC参数，增益和数据输出速率
*   形    参: _gain : 增益
*             _drate : 数据输出速率
*   返 回 值: 无
*********************************************************************************************************
*/
void ADS1256_CfgADC(ADS1256_GAIN_E _gain, ADS1256_DRATE_E _drate)
{
    g_tADS1256.Gain = _gain;
    g_tADS1256.DataRate = _drate;

    ADS1256_StopScan();         /* 暂停CPU中断 */

    ADS1256_ResetHard();       /* 硬件复位 */
    
    ADS1256_WaitDRDY();
    {
        uint8_t buf[4];     /* 暂存ADS1256 寄存器配置参数，之后连续写4个寄存器 */

        /* 状态寄存器定义
            Bits 7-4 ID3, ID2, ID1, ID0  Factory Programmed Identification Bits (Read Only)

            Bit 3 ORDER: Data Output Bit Order
                0 = Most Significant Bit First (default)
                1 = Least Significant Bit First
            Input data  is always shifted in most significant byte and bit first. Output data is always shifted out most significant
            byte first. The ORDER bit only controls the bit order of the output data within the byte.

            Bit 2 ACAL : Auto-Calibration
                0 = Auto-Calibration Disabled (default)
                1 = Auto-Calibration Enabled
            When Auto-Calibration is enabled, self-calibration begins at the completion of the WREG command that changes
            the PGA (bits 0-2 of ADCON register), DR (bits 7-0 in the DRATE register) or BUFEN (bit 1 in the STATUS register)
            values.

            Bit 1 BUFEN: Analog Input Buffer Enable
                0 = Buffer Disabled (default)
                1 = Buffer Enabled

            Bit 0 DRDY :  Data Ready (Read Only)
                This bit duplicates the state of the DRDY pin.

            ACAL=1使能自校准功能。当 PGA，BUFEEN, DRATE改变时会启动自校准
        */
        buf[0] = (0 << 3) | (1 << 2) | (1 << 1);
        //ADS1256_WriteReg(REG_STATUS, (0 << 3) | (1 << 2) | (1 << 1));

        buf[1] = 0x08;  /* 高四位0表示AINP接 AIN0,  低四位8表示 AINN 固定接 AINCOM */

        /*  ADCON: A/D Control Register (Address 02h)
            Bit 7 Reserved, always 0 (Read Only)
            Bits 6-5 CLK1, CLK0 : D0/CLKOUT Clock Out Rate Setting
                00 = Clock Out OFF
                01 = Clock Out Frequency = fCLKIN (default)
                10 = Clock Out Frequency = fCLKIN/2
                11 = Clock Out Frequency = fCLKIN/4
                When not using CLKOUT, it is recommended that it be turned off. These bits can only be reset using the RESET pin.

            Bits 4-2 SDCS1, SCDS0: Sensor Detect Current Sources
                00 = Sensor Detect OFF (default)
                01 = Sensor Detect Current = 0.5 μ A
                10 = Sensor Detect Current = 2 μ A
                11 = Sensor Detect Current = 10μ A
                The Sensor Detect Current Sources can be activated to verify  the integrity of an external sensor supplying a signal to the
                ADS1255/6. A shorted sensor produces a very small signal while an open-circuit sensor produces a very large signal.

            Bits 2-0 PGA2, PGA1, PGA0: Programmable Gain Amplifier Setting
                000 = 1 (default)
                001 = 2
                010 = 4
                011 = 8
                100 = 16
                101 = 32
                110 = 64
                111 = 64
        */
        buf[2] = (0 << 5) | (0 << 3) | (_gain << 0);
        //ADS1256_WriteReg(REG_ADCON, (0 << 5) | (0 << 2) | (GAIN_1 << 0)); /* 选择1;1增益, 输入正负5V */

        /* 因为切换通道和读数据耗时 123uS, 因此扫描中断模式工作时，最大速率 = DRATE_1000SPS */
        buf[3] = s_tabDataRate[_drate]; // DRATE_10SPS; /* 选择数据输出速率 */

        CS_0(); /* SPI片选 = 0 */
        SPI2_ReadWriteByte(CMD_WREG | 0); /* 写寄存器的命令, 并发送寄存器地址 */
        SPI2_ReadWriteByte(0x03);         /* 寄存器个数 - 1, 此处3表示写4个寄存器 */

        SPI2_ReadWriteByte(buf[0]);   /* 设置状态寄存器 */
        SPI2_ReadWriteByte(buf[1]);   /* 设置输入通道参数 */
        SPI2_ReadWriteByte(buf[2]);   /* 设置ADCON控制寄存器，增益 */
        SPI2_ReadWriteByte(buf[3]);   /* 设置输出数据速率 */

        CS_1(); /* SPI片选 = 1 */
    }

    bsp_DelayUS(50);
}


/*
*********************************************************************************************************
*   函 数 名: ADS1256_Init
*   功能说明: 初始化ADS1256
*   形    参: 无
*   返 回 值: 无
*********************************************************************************************************
*/
void ADS1256_Init(void)
{
    Init_ADS1256_GPIO();  /* ADS1256 IO的初始化  */    
    
    SPI2_Init();          /* SPI控制器初始化 */
}             

/*
*********************************************************************************************************
*   函 数 名: ADS1256_WriteReg
*   功能说明: 写指定的寄存器
*   形    参:  _RegID : 寄存器ID
*             _RegValue : 寄存器值
*   返 回 值: 无
*********************************************************************************************************
*/
static void ADS1256_WriteReg(uint8_t _RegID, uint8_t _RegValue)
{
    CS_0(); /* SPI片选 = 0 */
    SPI2_ReadWriteByte(CMD_WREG | _RegID);    /* 写寄存器的命令, 并发送寄存器地址 */
    SPI2_ReadWriteByte(0x00);     /* 寄存器个数 - 1, 此处写1个寄存器 */

    SPI2_ReadWriteByte(_RegValue);    /* 发送寄存器值 */
    CS_1(); /* SPI片选 = 1 */
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_ReadReg
*   功能说明: 写指定的寄存器
*   形    参:  _RegID : 寄存器ID
*             _RegValue : 寄存器值。
*   返 回 值: 读到的寄存器值。
*********************************************************************************************************
*/
static uint8_t ADS1256_ReadReg(uint8_t _RegID)
{
    uint8_t read;

    CS_0(); /* SPI片选 = 0 */
    SPI2_ReadWriteByte(CMD_RREG | _RegID);    /* 写寄存器的命令, 并发送寄存器地址 */
    SPI2_ReadWriteByte(0x00); /* 寄存器个数 - 1, 此处读1个寄存器 */

    ADS1256_DelayDATA();     /* 必须延迟才能读取芯片返回数据 */

    read = SPI2_ReadWriteByte(0x00);    /* 读寄存器值 */
    CS_1(); /* SPI片选 = 1 */

    return read;
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_WriteCmd
*   功能说明: 发送单字节命令
*   形    参:  _cmd : 命令
*   返 回 值: 无
*********************************************************************************************************
*/
static void ADS1256_WriteCmd(uint8_t _cmd)
{
    CS_0(); /* SPI片选 = 0 */
    SPI2_ReadWriteByte(_cmd);
    CS_1(); /* SPI片选 = 1 */
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_ReadChipID
*   功能说明: 读芯片ID, 读状态寄存器中的高4bit
*   形    参: 无
*   返 回 值: 8bit状态寄存器值的高4位
*********************************************************************************************************
*/
uint8_t ADS1256_ReadChipID(void)
{
    uint8_t id;

    ADS1256_WaitDRDY();
    id = ADS1256_ReadReg(REG_STATUS);
    return (id >> 4);
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_SetChannal
*   功能说明: 配置通道号。多路复用。AIN- 固定接地（ACOM).
*   形    参: _ch : 通道号， 0-7
*   返 回 值: 无
*********************************************************************************************************
*/
static void ADS1256_SetChannal(uint8_t _ch)
{
    /*
    Bits 7-4 PSEL3, PSEL2, PSEL1, PSEL0: Positive Input Channel (AINP) Select
        0000 = AIN0 (default)
        0001 = AIN1
        0010 = AIN2 (ADS1256 only)
        0011 = AIN3 (ADS1256 only)
        0100 = AIN4 (ADS1256 only)
        0101 = AIN5 (ADS1256 only)
        0110 = AIN6 (ADS1256 only)
        0111 = AIN7 (ADS1256 only)
        1xxx = AINCOM (when PSEL3 = 1, PSEL2, PSEL1, PSEL0 are “don’t care”)

        NOTE: When using an ADS1255 make sure to only select the available inputs.

    Bits 3-0 NSEL3, NSEL2, NSEL1, NSEL0: Negative Input Channel (AINN)Select
        0000 = AIN0
        0001 = AIN1 (default)
        0010 = AIN2 (ADS1256 only)
        0011 = AIN3 (ADS1256 only)
        0100 = AIN4 (ADS1256 only)
        0101 = AIN5 (ADS1256 only)
        0110 = AIN6 (ADS1256 only)
        0111 = AIN7 (ADS1256 only)
        1xxx = AINCOM (when NSEL3 = 1, NSEL2, NSEL1, NSEL0 are “don’t care”)
    */
    if (_ch > 7)
    {
        return;
    }
    ADS1256_WriteReg(REG_MUX, (_ch << 4) | (1 << 3));   /* Bit3 = 1, AINN 固定接 AINCOM */
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_SetDiffChannal
*   功能说明: 配置差分通道号。多路复用。
*   形    参: _ch : 通道号,0-3；共4对
*   返 回 值: 8bit状态寄存器值的高4位
*********************************************************************************************************
*/
static void ADS1256_SetDiffChannal(uint8_t _ch)
{
    /*
    Bits 7-4 PSEL3, PSEL2, PSEL1, PSEL0: Positive Input Channel (AINP) Select
        0000 = AIN0 (default)
        0001 = AIN1
        0010 = AIN2 (ADS1256 only)
        0011 = AIN3 (ADS1256 only)
        0100 = AIN4 (ADS1256 only)
        0101 = AIN5 (ADS1256 only)
        0110 = AIN6 (ADS1256 only)
        0111 = AIN7 (ADS1256 only)
        1xxx = AINCOM (when PSEL3 = 1, PSEL2, PSEL1, PSEL0 are “don’t care”)

        NOTE: When using an ADS1255 make sure to only select the available inputs.

    Bits 3-0 NSEL3, NSEL2, NSEL1, NSEL0: Negative Input Channel (AINN)Select
        0000 = AIN0
        0001 = AIN1 (default)
        0010 = AIN2 (ADS1256 only)
        0011 = AIN3 (ADS1256 only)
        0100 = AIN4 (ADS1256 only)
        0101 = AIN5 (ADS1256 only)
        0110 = AIN6 (ADS1256 only)
        0111 = AIN7 (ADS1256 only)
        1xxx = AINCOM (when NSEL3 = 1, NSEL2, NSEL1, NSEL0 are “don’t care”)
    */
    if (_ch == 0)
    {
        ADS1256_WriteReg(REG_MUX, (0 << 4) | 1);    /* 差分输入 AIN0， AIN1 */
    }
    else if (_ch == 1)
    {
        ADS1256_WriteReg(REG_MUX, (2 << 4) | 3);    /* 差分输入 AIN2， AIN3 */
    }
    else if (_ch == 2)
    {
        ADS1256_WriteReg(REG_MUX, (4 << 4) | 5);    /* 差分输入 AIN4， AIN5 */
    }
    else if (_ch == 3)
    {
        ADS1256_WriteReg(REG_MUX, (6 << 4) | 7);    /* 差分输入 AIN6， AIN7 */
    }
}


/*
*********************************************************************************************************
*   函 数 名: ADS1256_ReadData
*   功能说明: 读ADC 24位原始数据
*   形    参: 无
*   返 回 值: 无
*********************************************************************************************************
*/
static uint32_t ADS1256_ReadData(void)
{
    uint32_t read = 0;

    CS_0(); /* SPI片选 = 0 */

    SPI2_ReadWriteByte(CMD_RDATA);    /* 读数据的命令 */

    //ADS1256_DelayDATA();      /* 必须延迟才能读取芯片返回数据 */

    /* 读采样结果，3个字节，高字节在前 */
    read = SPI2_ReadWriteByte(0x00) << 16;
    read += (SPI2_ReadWriteByte(0x00) << 8);
    read += SPI2_ReadWriteByte(0x00);

    CS_1(); /* SPI片选 = 1 */
    
    return  read;
}



/*
*********************************************************************************************************
*   下面的函数用于DRDY中断工作模式
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*   函 数 名: ADS1256_GetAdc
*   功能说明: 从缓冲区读取ADC采样结果。采样结构是由中断服务程序填充的。
*   形    参: _ch 通道号 (0 - 7)
*   返 回 值: ADC采集结果（有符号数）
*********************************************************************************************************
*/
int32_t ADS1256_GetAdc(uint8_t _ch)
{
    uint32_t iTemp;

    if (_ch > 7)
    {
        return 0;
    }

    __set_PRIMASK(1);   /* 禁止中断 */

    iTemp = g_tADS1256.AdcNow[_ch];

    /* 负数进行扩展。24位有符号数扩展为32位有符号数 */
    if (iTemp & 0x800000)
    {
        iTemp += 0xFF000000;
    }
    
    __set_PRIMASK(0);   /* 使能中断 */

    return (int32_t)iTemp;
}

/*
*********************************************************************************************************
*   函 数 名: ADS1256_ISR
*   功能说明: 定时采集中断服务程序
*   形    参: 无
*   返 回 值: 无
*********************************************************************************************************
*/
void ADS1256_ISR(void)
{
    if (g_tADS1256.ScanMode == 0)   /* 0 表示单端8路扫描，1表示差分4路扫描 */
    {
        /* 读取采集结构，保存在全局变量 */
        ADS1256_SetChannal(g_tADS1256.Channel); /* 切换模拟通道 */

        ADS1256_WriteCmd(CMD_SYNC);

        ADS1256_WriteCmd(CMD_WAKEUP);

        if (g_tADS1256.Channel == 0)
        {
            g_tADS1256.AdcNow[7 - 4] = ADS1256_ReadData();  /* 注意保存的是上一个通道的数据 */
        }
        else
        {
            g_tADS1256.AdcNow[g_tADS1256.Channel-1] = ADS1256_ReadData();   /* 注意保存的是上一个通道的数据 */
        }

        if (++g_tADS1256.Channel >= (8 - 4))  /* 只测量4个通道 */
        {
            g_tADS1256.Channel = 0;
        }
        
    }
    else    /* 差分4路扫描 */
    {
        /* 读取采集结构，保存在全局变量 */
        ADS1256_SetDiffChannal(g_tADS1256.Channel); /* 切换模拟通道 */

        ADS1256_WriteCmd(CMD_SYNC);

        ADS1256_WriteCmd(CMD_WAKEUP);
        
        if (g_tADS1256.Channel == 0)
        {
            g_tADS1256.AdcNow[3 - 2] = ADS1256_ReadData();  /* 注意保存的是上一个通道的数据 */
        }
        else
        {
            g_tADS1256.AdcNow[g_tADS1256.Channel-1] = ADS1256_ReadData();   /* 注意保存的是上一个通道的数据 */
        }

        if (++g_tADS1256.Channel >= (4 - 2))
        {
            g_tADS1256.Channel = 0;
        }
    }
}

/*
*********************************************************************************************************
*   函 数 名: EXTI15_10_IRQHandler
*   功能说明: 外部中断服务程序.
*   形    参：无
*   返 回 值: 无
*********************************************************************************************************
*/
void EXTI15_10_IRQHandler(void)
{
    if (EXTI_GetITStatus(EXTI_Line10) != RESET)
    {
        EXTI_ClearITPendingBit(EXTI_Line10);     /* 清除中断标志位 */

        ADS1256_ISR();

        /* 执行上面的代码完毕后，再次清零中断标志 */
        EXTI_ClearITPendingBit(EXTI_Line10);     /* 清除中断标志位 */
    }
}

/* end of file */
