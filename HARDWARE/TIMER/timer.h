#ifndef __TIMER_H
#define __TIMER_H
#include "sys.h"


/* 通用定时器2中断初始化, period_num：自动重装值 */
void TIM2_Init(u16 period_num);

#endif
