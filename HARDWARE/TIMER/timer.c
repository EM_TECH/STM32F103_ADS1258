#include "timer.h"


/* 通用定时器2中断初始化, period_num：自动重装值 */
void TIM2_Init(u16 period_num)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    /* 基础设置，时基和比较输出设置，由于这里只需定时，所以不用OC比较输出 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    TIM_DeInit(TIM2);

    TIM_TimeBaseStructure.TIM_Period=period_num;   /* 装载值 */

    TIM_TimeBaseStructure.TIM_Prescaler=72-1;     /* 分频系数 */

    /* set clock division */
    TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1; 
    
    /* count up */
    TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up;

    TIM_TimeBaseInit(TIM2,&TIM_TimeBaseStructure);

    /* clear the TIM2 overflow interrupt flag */
    TIM_ClearFlag(TIM2,TIM_FLAG_Update);

    /* TIM2 overflow interrupt enable */
    TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);

    /* NVIC_PriorityGroup */
    NVIC_InitStructure.NVIC_IRQChannel=TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* enable TIM2 */
    TIM_Cmd(TIM2, ENABLE);
}










