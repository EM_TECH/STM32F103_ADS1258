#ifndef __SPI_H
#define __SPI_H
#include "sys.h"


/* SPI2 CS 高 */
#define SPI2_CS_H        GPIO_SetBits(GPIOB, GPIO_Pin_12)


/* SPI2 CS 低 */
#define SPI2_CS_L        GPIO_ResetBits(GPIOB, GPIO_Pin_12)


/* 初始化SPI2 */
void SPI2_Init(void);           

/* 设置SPI的速度 */
void SPI_SetSpeed(SPI_TypeDef *SPIx, uint8_t SpeedSet);

/* SPI2发送或者接收一字节 */
uint8_t SPI2_ReadWriteByte(uint8_t TxData);

#endif

