
#include "mcu_adc.h"

volatile uint16_t mcu_tin_ad[M];    /* AD转换结果缓存 */

static void RCC_Configuration(void)
{   
    /* 使能各个用到的外设时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_ADC1 | RCC_APB2Periph_AFIO, ENABLE); 

    RCC_ADCCLKConfig(RCC_PCLK2_Div6);   /* 72M/6=12,ADC最大时间不能超过14M */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);   /* 使能DMA传输 */
}

static void GPIO_AD_Configuration(void)
{
    /* 定义 GPIO 初始化结构体 GPIO_InitStructure */
    GPIO_InitTypeDef GPIO_InitStructure;
    
    /* PA3 ---> ADC通道3 */
    /* PA4 ---> ADC通道3 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init(GPIOA , &GPIO_InitStructure);
}


static void ADC_Configuration(void)
{
    /* 定义 ADC 初始化结构体 ADC_InitStructure */
    ADC_InitTypeDef ADC_InitStructure;

    ADC_DeInit(ADC1);      /* 将外设 ADC1 的全部寄存器重设为缺省值 */
   /*
    *   独立工作模式;
    *   多通道扫描模式;
    *   连续模数转换模式;
    *   转换触发方式：转换由软件触发启动;
    *   ADC 数据右对齐 ;
    *   进行规则转换的 ADC 通道的数目为10; 
    */
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;  /* ADC工作模式:工作在独立模式 */
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;        /* 扫描模式 */
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;  /* 循环模式 */
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; /* 由软件而不是外部触发启动 */
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;  /* ADC数据右对齐 */
    ADC_InitStructure.ADC_NbrOfChannel = M;     /* 顺序进行规则转换的ADC通道的数目 */
    ADC_Init(ADC1, &ADC_InitStructure);     /* 据ADC_InitStruct中指定的参数初始化外设ADCx的寄存器 */

    ADC_RegularChannelConfig(ADC1, ADC_Channel_3, 1, ADC_SampleTime_7Cycles5 );
    ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 2, ADC_SampleTime_7Cycles5 );
    
    /* 开启ADC的DMA支持（要实现DMA功能，还需独立配置DMA通道等参数）*/
    ADC_DMACmd(ADC1, ENABLE);
    /* 使能 ADC1 */
    ADC_Cmd(ADC1, ENABLE);
    /* 复位 ADC1 的校准寄存器 */   
    ADC_ResetCalibration(ADC1);
    /* 等待 ADC1 校准寄存器复位完成 */
    while(ADC_GetResetCalibrationStatus(ADC1));
    /* 开始 ADC1 校准 */
    ADC_StartCalibration(ADC1);
    /* 等待 ADC1 校准完成 */
    while(ADC_GetCalibrationStatus(ADC1));
}


static void DMA_Configuration(void)
{
    DMA_InitTypeDef DMA_InitStructure;
    DMA_DeInit(DMA1_Channel1);    /* 将DMA的通道1寄存器重设为缺省值 */
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)&ADC1->DR;  /* DMA外设ADC基地址 */
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)mcu_tin_ad;     /* DMA内存基地址 */
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;   /* 内存作为数据传输的目的地 */
    DMA_InitStructure.DMA_BufferSize = M;                /* DMA通道的DMA缓存的大小 */
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;  /* 外设地址寄存器不变 */
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;  /* 内存地址寄存器递增 */
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;  /* 数据宽度为16位 */
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;  /* 数据宽度为16位 */
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;      /* 工作在循环缓存模式 */
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;  /* DMA通道 x拥有高优先级 */
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  /* DMA通道x没有设置为内存到内存传输 */
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);  /* 根据DMA_InitStruct中指定的参数初始化DMA的通道 */
}


void ADC1_Init (void)
{
    RCC_Configuration();      /* 使能外设时钟 */
    GPIO_AD_Configuration();  /* AD模块GPIO设置 */
    ADC_Configuration();      /* AD配置 */
    DMA_Configuration();      /* DMA配置 */
 
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);   /* 启动 ADC1 转换 */
    DMA_Cmd(DMA1_Channel1, ENABLE);           /* 启动 DMA 通道 */
}

/* end of file */
