
#ifndef __MUC_ADC_H
#define __MUC_ADC_H

#include "sys.h"
#include "stdlib.h"


#define    M     2                         /*　通道数 */
extern volatile uint16_t mcu_tin_ad[M];    /* AD转换结果缓存 */

/**
 * \brief   ADC1初始化
 *
 * \param   无
 *
 * \return  无
 */
void ADC1_Init (void);


#endif   /* __MUC_ADC_H */
     
/* end of file */

