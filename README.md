# STM32F103_ADS1258

#### 介绍
STM32F103C8T7-LQFP48 MCU驱动ADS1258，具体需求如下：
1. STM32通过SPI2口控制24位AD转换器采样，采样2路差分信号，外部信号电压正负2.5V；
2. STM32通过自身12位AD(PA3\PA4)采样两路模拟信号信号,电压0-2.5V；
3. 采样回来的数据整理后通过串口USART1(RS422)向外发送，波特率按230.4K,1起始位、8数据位、无校验；
4. 系统工作频率（完成采样和发送）应在400HZ左右;
5. 发送数据内容可以按外部串口USART1（RS422）接收的指令变化，指令暂定“A””B””C”；
6. ADS1258程序应可以扩展更多路采样，输入的IN14和IN15作为参考电压源的AD采样测试用；

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)