
#ifndef __DATA_TRANS_H
#define __DATA_TRANS_H

#include "sys.h"

/* 帧类型0XFA 单端型 */
void data_trans_dingle_ended(float *p_ads_volt, float *p_mcu_ad_volt);


/* 帧类型0XFB 不转换 */
void data_trans_not_convert(int32_t *p_ads_diff_val, int8_t temp_1, int8_t temp_2);


/* 帧类型0XFC  差分型 */
void data_trans_difference(float *p_ads_diff_volt, int8_t temp_1, int8_t temp_2);


#endif    /* __DATA_TRANS_H */


/* end of file */



