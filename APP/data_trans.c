#include "data_trans.h"
#include "usart.h"
#include "ads1256.h"
#include "string.h"


/* 帧类型0XFA 单端型 */
void data_trans_dingle_ended(float *p_ads_volt, float *p_mcu_ad_volt)
{
    uint8_t send_buf[32] = {0};
    uint8_t cnt = 0;
    uint8_t i   = 0;
    uint8_t xor_val = 0;
    
    send_buf[cnt++] = 0xF1;
    send_buf[cnt++] = 0xFA;
    
    memcpy(&send_buf[cnt], p_ads_volt, 16);
    cnt += 16;
    
    memcpy(&send_buf[cnt], p_mcu_ad_volt, 8);
    cnt += 8;
    
    xor_val = send_buf[0];
    for (i = 1; i < cnt; i++) {
        xor_val ^= send_buf[i]; 
    }
    send_buf[cnt++] = xor_val;
    
    uart1_send_bytes(send_buf, cnt);  
}


/* 帧类型0XFB 不转换 */
void data_trans_not_convert(int32_t *p_ads_diff_val, int8_t temp_1, int8_t temp_2)
{
    uint8_t send_buf[32] = {0};
    uint8_t cnt = 0;
    uint8_t i   = 0;
    uint8_t xor_val = 0;
    
    send_buf[cnt++] = 0xF1;
    send_buf[cnt++] = 0xFB;
    
    send_buf[cnt++] = p_ads_diff_val[0] >> 16;
    send_buf[cnt++] = p_ads_diff_val[0] >> 8;
    send_buf[cnt++] = p_ads_diff_val[0];
    
    send_buf[cnt++] = temp_1;

    send_buf[cnt++] = p_ads_diff_val[1] >> 16;
    send_buf[cnt++] = p_ads_diff_val[1] >> 8;
    send_buf[cnt++] = p_ads_diff_val[1];
    
    send_buf[cnt++] = temp_2;
    
    xor_val = send_buf[0];
    for (i = 1; i < cnt; i++) {
        xor_val ^= send_buf[i]; 
    }
    send_buf[cnt++] = xor_val;
    
    uart1_send_bytes(send_buf, cnt);    
}


/* 帧类型0XFC  差分型 */
void data_trans_difference(float *p_ads_diff_volt, int8_t temp_1, int8_t temp_2)
{
    uint8_t send_buf[32] = {0};
    uint8_t cnt = 0;
    uint8_t i   = 0;
    uint8_t xor_val = 0;
    
    send_buf[cnt++] = 0xF1;
    send_buf[cnt++] = 0xFC;
   
    memcpy(&send_buf[cnt], &p_ads_diff_volt[0], 4);
    cnt += 4;

    send_buf[cnt++] = temp_1;

    memcpy(&send_buf[cnt], &p_ads_diff_volt[1], 4);
    cnt += 4;
   
    send_buf[cnt++] = temp_2;
    
    xor_val = send_buf[0];
    for (i = 1; i < cnt; i++) {
        xor_val ^= send_buf[i]; 
    }
    send_buf[cnt++] = xor_val;
    
    uart1_send_bytes(send_buf, cnt); 
}



/* end of file */
